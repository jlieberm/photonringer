# The name of the package:

atlas_subdir(PhotonRingMatchTool)

# Add the shared library

atlas_add_library(PhotonRingMatchToolLib
PhotonRingMatchTool/*.h Root/*.cxx
	PUBLIC_HEADERS PhotonRingMatchTool
	LINK_LIBRARIES AnaAlgorithmLib xAODEventInfo xAODJet xAODTrigRinger
			xAODTruth xAODEgamma xAODTrigEgamma xAODMuon TrigDecisionToolLib TriggerMatchingToolLib
			TrigNavStructure)

atlas_depends_on_subdirs( PUBLIC
                         )
			
if(XAOD_STANDALONE)

	atlas_add_dictionary(PhotonRingMatchToolDict
		PhotonRingMatchTool/PhotonRingMatchTool.h
		PhotonRingMatchTool/selection.xml
		LINK_LIBRARIES PhotonRingMatchToolLib )
endif()

if (NOT XAOD_STANDALONE)
	atlas_add_component (PhotonRingMatchTool
		src/components/*.cxx
		LINK_LIBRARIES PhotonRingMatchToolLib)
endif()

atlas_install_joboptions( share/*_jobOptions.py)
atlas_install_scripts( share/*_eljob.py)

