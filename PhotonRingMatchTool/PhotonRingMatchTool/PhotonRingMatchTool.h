#ifndef PhotonRingMatchTool_PhotonRingMatchTool_H
#define PhotonRingMatchTool_PhotonRingMatchTool_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>
#include <xAODTrigRinger/versions/TrigRingerRings_v2.h>
#include <xAODTruth/versions/TruthParticle_v1.h>
#include "AsgTools/AnaToolHandle.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
class PhotonRingMatchTool : public EL::AnaAlgorithm{

	public:
		PhotonRingMatchTool(const std::string& name, ISvcLocator* pSvcLocator);

		virtual StatusCode initialize () override;
		virtual StatusCode execute () override;
		virtual StatusCode finalize () override;

	private:
		double m_electronPtCut;
		std::string m_sampleName;
		int m_Nphotons;
		asg::AnaToolHandle<Trig::TrigDecisionTool> m_tdt;
		asg::AnaToolHandle<Trig::MatchingTool> m_tmt;
		// float m_cutValue;
		// TTree *m_myTree;
		// TH1 *m_myHist;
	};

#endif
