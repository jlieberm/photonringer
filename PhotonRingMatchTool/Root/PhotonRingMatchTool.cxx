#include <AsgTools/MessageCheck.h>
#include <PhotonRingMatchTool/PhotonRingMatchTool.h>
#include <xAODEventInfo/EventInfo.h>

#include <xAODTrigRinger/versions/TrigRingerRings_v2.h>
#include <xAODTrigRinger/TrigRingerRingsContainer.h>
#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODEgamma/Photon.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODTruth/xAODTruthHelpers.h>
#include <xAODTrigEgamma/TrigPhoton.h>
#include <xAODTrigEgamma/TrigPhotonContainer.h>
#include <TrigNavStructure/TriggerElement.h>
//#include <TrigEgammaMatchingTool/TrigEgammaMatchingTool.h>


PhotonRingMatchTool :: PhotonRingMatchTool (const std::string& name, ISvcLocator *pSvcLocator) 
	: EL::AnaAlgorithm(name, pSvcLocator) {
			declareProperty( "ElectronPtCut", m_electronPtCut = 25000.0, "Minimum electron pT (in MeV)");
			declareProperty( "SampleName", m_sampleName = "Unknown", "Descriptive name for the processed sample" );
			declareProperty( "Nphotons", m_Nphotons = 0, "Start Photon Counter");
			
			// Here you put any code for the base initialization of variables,
			// e.g. initialize all pointers to 0.  This is also where you
			// declare all properties for your algorithm.  Note that things like
			// resetting statistics variables or booking histograms should
			// rather go into the initialize() function.
	}

StatusCode PhotonRingMatchTool :: initialize () {

	// Here you do everything that needs to be done at the very
	// beginning on each worker node, e.g. create histograms and output
	// trees.  This method gets called before any input files are
	// connected.
	ANA_MSG_INFO("in initialize");
	ANA_MSG_INFO( "ElectronPtCut 	= " << m_electronPtCut );
	ANA_MSG_INFO( "SampleName 	= " << m_sampleName ); 
	ANA_MSG_INFO( "Nphotons 	= " << m_Nphotons );
	m_tdt.setTypeAndName("Trig::TrigDecisionTool/TrigDecisionTool");
	CHECK( m_tdt.initialize() );
	return StatusCode::SUCCESS;
	}

StatusCode PhotonRingMatchTool :: execute (){
	// Here you do everything that needs to be done on every single
	// events, e.g. read input variables, apply cuts, and fill
	// histograms and trees.  This is where most of your actual analysis
	// code will go.
	//ANA_MSG_INFO("in execute");
	const xAOD::EventInfo *eventInfo = nullptr;
	const xAOD::PhotonContainer* photons = nullptr;
	const xAOD::TrigRingerRingsContainer* ringers = nullptr;
	
	ANA_CHECK(evtStore()->retrieve(eventInfo, "EventInfo"));
	ANA_CHECK(evtStore()->retrieve(ringers,"HLT_xAOD__TrigRingerRingsContainer_TrigT2CaloEgamma"));
	ANA_CHECK(evtStore()->retrieve(photons,"Photons"))

	for (const xAOD::Photon *photon : *photons){
		if (xAOD::TruthHelpers::getTruthParticle(*photon)!=0){
			const xAOD::TruthParticle* particle = xAOD::TruthHelpers::getTruthParticle(*photon);
			if (particle->isPhoton()){
				
			}
		}
	}
	
	// for (const xAOD::TrigRingerRings_v2 *r : *ringers){
	// 	const xAOD::TrigEMCluster_v1 *emCluster = r->emCluster();
	// 	if ((emCluster->eta() == p->eta()) && (emCluster->phi() == p->phi())){
	// 		ANA_MSG_INFO("execute(): There is a match between ringer and photon!");
	// 	}
	// }		
	// auto allChains = m_tdt->getChainGroup(".*");
	// std::cout << "Triggers that passed : ";
	// for(auto& trig : allChains->getListOfTriggers()) if(m_tdt->getChainGroup(trig)->isPassed()) std::cout << trig << ", ";
	// std::cout << std::endl;
	return StatusCode::SUCCESS;
}

	
	


StatusCode PhotonRingMatchTool :: finalize (){

	// This method is the mirror image of initialize(), meanining it gets
	// called after the last event has been processed on the worker node
	// and allows you to finish up any objects you created in
	// initialize() before they are written to disk.  This is actually
	// fairly rare, since this happens separately for each worker node.
	// Most of the time you want to do your post-processing on the
	// submission node after all your histogram outputs have been
       	// merged.
       	ANA_MSG_INFO("Total of photons: " << m_Nphotons);
	return StatusCode::SUCCESS;
	}
