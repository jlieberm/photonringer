#!/bin/sh
mkdir ../buildPhotonRinger
cd ../buildPhotonRinger
setupATLAS
asetup 21.2.104,AthAnalysis
cmake ../photonringer/
make
source x86*/setup.sh
export ALRB_TutorialData=/afs/cern.ch/atlas/project/PAT/tutorial/cern-jan2018/
cd ../photonringer/PhotonRingMatchTool/share/
